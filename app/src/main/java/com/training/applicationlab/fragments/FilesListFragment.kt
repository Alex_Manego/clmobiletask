package com.training.applicationlab.fragments

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.core.view.doOnPreDraw
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.training.applicationlab.R
import com.training.applicationlab.adapters.FilesRvAdapter
import com.training.applicationlab.model.File
import com.training.applicationlab.viewmodel.FilesViewModel


class FilesListFragment : Fragment(), FilesRvAdapter.OnItemClickListener {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_files_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postponeEnterTransition()
        view.doOnPreDraw { startPostponedEnterTransition() }
        val viewModel = ViewModelProvider(requireActivity()).get(FilesViewModel::class.java)
        val rv = view.findViewById<RecyclerView>(R.id.rv_files)
        val myAdapter = FilesRvAdapter(this)
        rv.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = myAdapter
        }
        viewModel.getFilesList()
        viewModel.filesList.observe(viewLifecycleOwner) { list ->
            myAdapter.setFilesList(list)
        }
    }

    override fun onItemClick(file: File?, view: ImageView) {
        val bundle = Bundle()
        val imageViewSize = requireActivity().resources.getDimension(R.dimen.image_size).toInt()
        bundle.putString(ARG_TITLE, file?.title)
        bundle.putString(ARG_DESCRIPTION, file?.description)
        bundle.putInt(ARG_SIZE_IMAGEVIEW, imageViewSize)
        val extras = FragmentNavigatorExtras(view to file!!.title)
        findNavController().navigate(
            R.id.action_filesListFragment_to_descriptionFragment,
            bundle,
            null,
            extras
        )
    }

}
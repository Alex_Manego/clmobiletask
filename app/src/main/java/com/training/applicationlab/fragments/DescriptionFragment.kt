package com.training.applicationlab.fragments

import android.os.Build
import android.os.Bundle
import androidx.transition.TransitionInflater
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.Toolbar
import androidx.core.view.ViewCompat
import androidx.core.view.updateLayoutParams
import androidx.navigation.fragment.findNavController
import com.training.applicationlab.R


const val ARG_TITLE = "title_arg"
const val ARG_DESCRIPTION = "description_arg"
const val ARG_SIZE_IMAGEVIEW = "size_arg"

class DescriptionFragment : Fragment() {
    private var title: String? = null
    private var description: String? = null
    private var size_imageView: Int? = null
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            title = it.getString(ARG_TITLE)
            description = it.getString(ARG_DESCRIPTION)
            size_imageView = it.getInt(ARG_SIZE_IMAGEVIEW) * 2
        }
        sharedElementEnterTransition = TransitionInflater.from(requireContext())
            .inflateTransition(R.transition.shared_transition)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_description, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar = view.findViewById(R.id.my_toolbar)
        val titleTextView = view.findViewById<TextView>(R.id.secondFragment_titleTextView)
        val descTextView = view.findViewById<TextView>(R.id.secondFragment_descriptionTextView)
        val imageView = view.findViewById<ImageView>(R.id.secondFragment_imageView)
        imageView.apply {
            updateLayoutParams {
                width = size_imageView ?: 120
                height = size_imageView ?: 120
            }
            ViewCompat.setTransitionName(this, title)
        }
        setActionToolbarItems()
        titleTextView.text = title
        descTextView.text = description
    }

    private fun setActionToolbarItems() {
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_back -> {
                    findNavController().navigateUp()
                    true
                }
                R.id.action_close -> {
                    activity?.finish()
                    System.exit(0)
                    true
                }
                else -> false
            }
        }
    }
}

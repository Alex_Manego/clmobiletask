package com.training.applicationlab.model

data class File(val title: String, val description: String)

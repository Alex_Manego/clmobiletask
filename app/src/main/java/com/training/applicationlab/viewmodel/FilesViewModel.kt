package com.training.applicationlab.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.training.applicationlab.model.File

class FilesViewModel : ViewModel() {
    private var mutableList: MutableList<File> = mutableListOf()

    private val _filesList = MutableLiveData<List<File>>()
    val filesList: LiveData<List<File>> get() = _filesList

    fun getFilesList() {
        for (i in 1..1000) {
            mutableList.add(File(title = "Title $i", description = "Description $i"))
        }
        _filesList.value = mutableList
    }
}
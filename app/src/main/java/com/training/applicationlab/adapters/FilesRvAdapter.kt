package com.training.applicationlab.adapters

import android.annotation.SuppressLint
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.training.applicationlab.R
import com.training.applicationlab.model.File

class FilesRvAdapter(listener: OnItemClickListener) :
    RecyclerView.Adapter<FilesRvAdapter.MyHolder>() {
    private var mutableList: MutableList<File>? = mutableListOf()
    private var listener: OnItemClickListener? = null

    init {
        this.listener = listener
    }

    interface OnItemClickListener {
        fun onItemClick(file: File?, view: ImageView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item, parent, false)
        return MyHolder(itemView = itemView)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.title.text = mutableList?.get(position)?.title ?: "null"
        holder.description.text = mutableList?.get(position)?.description ?: "null"
        ViewCompat.setTransitionName(holder.imageView, mutableList?.get(position)?.title)
        holder.itemView.setOnClickListener {
            listener?.onItemClick(
                mutableList?.get(position),
                holder.imageView
            )
        }
    }

    override fun getItemCount(): Int {
        return mutableList?.size ?: 0
    }


    @SuppressLint("NotifyDataSetChanged")
    fun setFilesList(list: List<File>) {
        mutableList?.clear()
        mutableList?.addAll(list)
        notifyDataSetChanged()
    }

    inner class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<TextView>(R.id.title_textview)
        val description = itemView.findViewById<TextView>(R.id.description_textview)
        val imageView = itemView.findViewById<ImageView>(R.id.items_imageView)
    }
}